#ifndef ADDER_HPP
#define ADDER_HPP

SC_MODULE(Adder) {
	static const unsigned DATA_WIDTH = 16;
		
	sc_in<sc_lv<DATA_WIDTH> > op1;
	sc_in<sc_lv<DATA_WIDTH> > op2;
	sc_out<sc_lv<DATA_WIDTH> > result;

	SC_CTOR(Adder) {
		SC_THREAD(update_ports);
			sensitive << op1 << op2;
			dont_initialize();
	}
	
	private:
	void update_ports();
};

#endif
