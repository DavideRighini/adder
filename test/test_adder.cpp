#include <systemc.h>
#include "adder.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
sc_fifo<int> observer_adder;	
int values1[TEST_SIZE]; //vettore usato per tutti i test
int values2[TEST_SIZE]; //vettore usato per tutti i test

SC_MODULE(TestBench) 
{
 public:
 
	//canali per Adder
	sc_signal<sc_lv<DATA_WIDTH> > adder_op1;
	sc_signal<sc_lv<DATA_WIDTH> > adder_op2;
	sc_signal<sc_lv<DATA_WIDTH> > adder_result;	

	Adder adder1 ; //ADDER

  SC_CTOR(TestBench) : adder1("adder1")
  {
 		init_values();
 		
 		SC_THREAD(init_values_adder_test); 
		adder1.op1(this->adder_op1); 
		adder1.op2(this->adder_op2);        
		adder1.result(this->adder_result);
		SC_THREAD(observer_thread_adder);
		  sensitive << adder1.result;	

  }

	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
				
				values2[0] = 1;
				values2[1] = 3;
				values2[2] = 5;
				values2[3] = 7;
				values2[4] = 9;
				values2[5] = 0;
						  		  
	}



	void init_values_adder_test() {
		//inizializzazione adder
		for (unsigned i=0;i<TEST_SIZE;i++) {
		  adder_op1.write(values1[i]); 
		  adder_op2.write(values2[i]);   
		  wait(10,SC_NS);
		}
		wait(50,SC_NS);
		sc_stop();
	}

	void observer_thread_adder(){
		while(true) {
				wait();
				int value = adder1.result->read().to_uint();
				cout << "observer_thread: at " << sc_time_stamp() << " adder out: " << value << endl;
				if (observer_adder.num_free()>0 ) observer_adder.write(value);
		} 
	}

	int check_adder() {
		int test=0;
		for (unsigned i=0;i<TEST_SIZE;i++) {
				test=observer_adder.read(); //porta fifo
				//cout << "test: " << test << "\n";
		    if (test != (values1[i] + values2[i]) )
		        return 1;
		}                 

		return 0;
	}

};

// ---------------------------------------------------------------------------------------

int sc_main(int argc, char* argv[]) {
	
	TestBench testbench("testbench");
	sc_start();
	
  return testbench.check_adder();
}
