#include <systemc.h>
#include "adder.hpp"

void Adder::update_ports() {
	sc_lv<DATA_WIDTH> tmp;

	while(true) {
		tmp = op1->read().to_int() + op2->read().to_int() ;
		result->write(tmp);	
		wait();	
	}
}
